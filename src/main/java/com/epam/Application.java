package com.epam;

import java.util.InputMismatchException;
import java.util.Scanner;


/**
 * <h1>My first Maven project!</h1>
 * This is the application where you can find all odd and even numbers
 * and theirs sum in the specified interval and prints the result on the screen
 * Also you can specify the interval to make Fibonacci numbers set, which
 * will be printed on screen. The application will find biggest odd and
 * even numbers, and find percentage of odd and even numbers in the
 * Fibonacci numbers set.
 *
 * @author Dovganyuk Taras
 * @version 1.0
 * @since 08.11.2019
 */
public final class Application {

    /**
     * Main constructor of class.
     */
    private Application() {
    }

    /**
     *This is main method, where the application starts.
     * @param args this is the parameter of main method.
     */
    public static void main(final String[] args) {
                int startInt;
        int finishInt;

        /**
         * Try/catch block, which read interval entered and
         * throws an exception when format of numbers entered is invalid
         * Also this method prints odd and even numbers in interval entered,
         * calculate their sum.
         */
          try {
            Scanner input = new Scanner(System.in);
            System.out.println("Please enter the interval (two numbers): ");
            startInt = input.nextInt();
            finishInt = input.nextInt();

              if (finishInt < startInt) {
                  int x = startInt;
                  startInt = finishInt;
                  finishInt = x;
              }
            System.out.println("The interval entered: ["
                    + startInt + ", " + finishInt + "]");

              System.out.println("\nOdd numbers:");
              int sumOdds = 0;
              int sumEvens = 0;
            for (int i = startInt; i <= finishInt; i++) {
                if (i % 2 == 0) {
                    System.out.print(i + " ");
                    sumOdds += i;
                }
            }
              System.out.println("\nSum of odd numbers: " + sumOdds);

              System.out.println("\nEven numbers:");
              for (int i = startInt; i <= finishInt; i++) {
                  if (i % 2 != 0) {
                      System.out.print(i + " ");
                      sumEvens += i;
                  }
              }
              System.out.println("\nSum of even numbers: " + sumEvens);

        } catch (InputMismatchException e) {
            System.out.println("\nInvalid format of number");
        }

          Fibonacci newSet = new Fibonacci();
          newSet.printSet();
          newSet.findBiggestOddNumber();
          newSet.findBiggestEvenNumber();

        }

    }
