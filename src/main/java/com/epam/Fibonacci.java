package com.epam;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.InputMismatchException;

/**
 * Fibonacci class describes the Fibonacci numbers sequence, where
 * each number is the sum of the two preceding ones. N is a number
 * witch describes how many numbers are in Fibonacci sequence.
 */
class Fibonacci {
    /**
     * n1 is the first number of Fibonacci sequence.
      */
    private long n1 = 0;
    /**
     * n2 is the second number of Fibonacci sequence.
     */
    private long n2 = 1;
    /**
     * numberList is a list of numbers of Fibonacci sequence.
     */
    private List<Long> numberList = new LinkedList<>();
    /**
     * Hundred is static variable for calculating percentage.
     */
    private static final int HUNDRED = 100;

    /**
     * The main constructor of the Fibonacci class.
     */
 Fibonacci() {
    }

    /**
     * The method receive N from console and builds Fibonacci sequence
     * of N numbers. Also print it.
     */
      void printSet() {
          /**
           * Try/catch block receive entered N and check format of entered
           * number. Throws exception if format is invalid.
           */
        try {
            Scanner input = new Scanner(System.in);
            System.out.println("\nPlease enter size of Fibonacci numbers set:");
            int sizeOfSet = input.nextInt();


            numberList.add(n1);
            numberList.add(n2);
            for (int i = 2; i < sizeOfSet; i++) {
                long n3 = n1 + n2;
                numberList.add(n3);
                n1 = n2;
                n2 = n3;
            }
            System.out.println("\nFibonacci numbers: " + numberList);
        } catch (InputMismatchException e) {
            System.out.println("\nInvalid format of number");
        }
    }

    /**
     * The method calculates percentage of amount of total.
     * @param amount is amount of Odd or Even numbers in sequence.
     * @param total is total numbers in sequence.
     * @return percentage of amount of total.
     */
    private float calculatePercentage(final int amount, final int total) {
     return (float) amount * HUNDRED / total;
    }

    /**
     * The method finds biggest odd number in sequence and prints it.
     */
    void findBiggestOddNumber() {
        long max = 0;
        int amount = 0;

        for (Long num : numberList) {
            if (num % 2 == 0 && num > max) {
                max = num;
                amount += 1;
            }
        }

            System.out.println("\nF1 is: " + max + "\nOdd's %: "
                    + calculatePercentage(amount, numberList.size()));
        }

    /**
     * The method finds biggest even number in sequence and prints it.
     */
    void findBiggestEvenNumber() {
        long max = 0;
        int amount = 1;

        for (Long num : numberList) {
            if (num % 2 != 0 && num > max) {
                max = num;
                amount += 1;
            }
        }
            System.out.println("\nF2 is: " + max + "\nEvens' %: "
                    + calculatePercentage(amount, numberList.size()));
    }
}
